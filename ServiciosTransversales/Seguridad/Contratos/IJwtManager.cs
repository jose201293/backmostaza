﻿using Mostaza.Dominio.Core.Entidades;
using ServiciosTransversales.Seguridad.Modelos;
using System.Threading.Tasks;

namespace ServiciosTransversales.Seguridad.Contratos
{
    public interface IJwtManager
    {
        Token GenerarToken(Usuarios usuario);
        Task<Token> GenerarToken(string correo, string contrasenia);
        string EncriptarContrasenia(string contrasenia);
    }
}