﻿using Microsoft.IdentityModel.Tokens;
using Mostaza.Dominio.Core.ContratosRepositorios;
using Mostaza.Dominio.Core.Entidades;
using ServiciosTransversales.Seguridad.Contratos;
using ServiciosTransversales.Seguridad.Modelos;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ServiciosTransversales.Seguridad.Implementaciones
{
    public class JwtManager : IJwtManager
    {
        
        private readonly IUsuarioRepositorio _perfilRepositorio;
        public JwtManager(IUsuarioRepositorio perfilRepositorio)
        {
            _perfilRepositorio = perfilRepositorio;
        }
        public async Task<Token> GenerarToken(string correo, string contrasenia)
        {

            var usuario = await ValidarUsuarioAsync(correo, EncriptarContrasenia(contrasenia));
            if (usuario == null)
            {
                return null;
            }
            return ConfigurarToken(usuario);
        }

        public Token GenerarToken(Usuarios usuario)
        {
            return ConfigurarToken(usuario);
        }
        public Token ConfigurarToken(Usuarios usuario)
        {
            int duracionMinutosToken = 20;
            //se debe cambiar
            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("hasjkdhkasybua879879casadqwebbbdhsahdsalaoajas8556hsbdasjjsjs"));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);
            var header = new JwtHeader(signingCredentials);

            var claims = GenerarClaims(usuario);
            var payload = GenerarPayLoad(claims, duracionMinutosToken);
            var token = new JwtSecurityToken(header, payload);

            return new Token
            {
                ExpirationMinutes = duracionMinutosToken,
                TokenValue = new JwtSecurityTokenHandler().WriteToken(token)
            };
        }
        private async Task<Usuarios> ValidarUsuarioAsync(string correo, string contrasenia)
        {
            var usuarioValidado = await _perfilRepositorio.GetUsuarioAsync(x => x.Correo == correo && x.Contrasenia == EncriptarContrasenia(contrasenia));
            if (usuarioValidado != null)
            {
                return usuarioValidado;

            }
            return null;
        }
        public string EncriptarContrasenia(string contrasenia)
        {
            return Cifrador.Encrypt(contrasenia,null);
        }
        private Claim[] GenerarClaims( Usuarios usuario) => new []
        {
            new Claim("Correo", usuario.Correo),
            new Claim("TipoUsuario", usuario.IdTipoUsuario.ToString()),
            new Claim("Id", usuario.Perfil.IdPerfil.ToString()),// es el id del perfil

        };
        private JwtPayload GenerarPayLoad(Claim []claims,int duracionMinutosToken) => new JwtPayload
            (
               "http://localhost:8098",
               "http://localhost:8098",
               claims,
               DateTime.Now,
               DateTime.UtcNow.AddMinutes(duracionMinutosToken)
           );

    }
}
