﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ServiciosTransversales.Seguridad.Modelos
{
    public class Token
    {
        public string TokenValue { get; set; }
        public int ExpirationMinutes { get; set; }

    }
}
