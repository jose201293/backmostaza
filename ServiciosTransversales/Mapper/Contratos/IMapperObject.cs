﻿using System.Collections.Generic;
using AutoMapper;

namespace ServiciosTransversales.Mapper.Contratos
{
    public interface IMapperObject
    {
        U CreateMap<T, U>(object destination);
        U CreateMap<U>(object destination, MapperConfiguration mapperConfiguration);
        List<U> CreateMapCollection<T, U>(List<T> collection);
    }
}