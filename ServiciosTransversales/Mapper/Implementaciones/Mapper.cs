﻿using AutoMapper;
using ServiciosTransversales.Mapper.Contratos;
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiciosTransversales.Mapper.Implementaciones
{
    public class Mapper : IMapperObject
    {
        /// <summary>
        /// Realiza un mapeo de una entidad a otra
        /// </summary>
        /// <typeparam name="T">Origen</typeparam>
        /// <typeparam name="U">Destino</typeparam>
        /// <param name="origen">Objeto a transformar</param>
        /// <returns></returns>
        public U CreateMap<T, U>(object origen)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<T, U>());
            var mapper = config.CreateMapper();
            U destino = mapper.Map<U>(origen);
            return destino;
        }
        /// <summary>
        /// Realiza un mapeo de una entidad a otra donde sus campos no son iguales
        /// </summary>
        /// <typeparam name="U"></typeparam>
        /// <param name="destination"></param>
        /// <param name="mapperConfiguration">Objeto que tiene la configuraciónal mapeo que se intenta realizar</param>
        /// <returns></returns>
        public U CreateMap<U>(object destination, MapperConfiguration mapperConfiguration)
        {
            var mapper = mapperConfiguration.CreateMapper();
            U dto = mapper.Map<U>(destination);
            return dto;
        }
        public List<U> CreateMapCollection<T, U>(List<T> collection)
        {
            var config = new MapperConfiguration(cfg => cfg.CreateMap<T, U>());
            var mapper = config.CreateMapper();
            return mapper.Map<List<U>>(collection);

        }
    }
}
