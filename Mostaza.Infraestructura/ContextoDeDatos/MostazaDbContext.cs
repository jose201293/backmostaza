﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Mostaza.Dominio.Core.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace Mostaza.Infraestructura.ContextoDeDatos
{
    public class MostazaDbContext: DbContext
    {
        private const string connectionString = "Server=localhost;Database=dbMostaza;Trusted_Connection=True;";

        public DbSet<Usuarios> Usuarios { get; set; }
        public DbSet<Perfiles> Perfiles { get; set; }
        public MostazaDbContext()
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Usuarios>()
                .HasOne(u => u.Perfil)
                .WithOne(b => b.Usuario)
                .HasForeignKey<Perfiles>(b => b.IdUsuario);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(connectionString);
        }


    }
}
