﻿using Mostaza.Dominio.Core.ContratosRepositorios;
using Mostaza.Dominio.Core.Entidades;
using Mostaza.Dominio.Core.UnidadDeTrabajo;
using Mostaza.Infraestructura.ContextoDeDatos;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;


namespace Mostaza.Infraestructura.ImplementacionRepositorios
{
    public class PerfilRepositorio : IPerfilRepositorio
    {
        private readonly MostazaDbContext _contexto;
        public PerfilRepositorio(MostazaDbContext contexto)
        {
            _contexto = contexto;
        }
        public async Task<Perfiles> CrearPerfilAsync(Perfiles perfil)
        {
            var perfilContext = await _contexto.AddAsync(perfil);

            return perfilContext.Entity;
        }

        public void EliminarPerfil(int id)
        {
            throw new NotImplementedException();
        }

        public List<Perfiles> GetPerfil(Expression<Func<Usuarios, bool>> predicado)
        {
            throw new NotImplementedException();
        }
    }
}
