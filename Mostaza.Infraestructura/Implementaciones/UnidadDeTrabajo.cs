﻿using Microsoft.EntityFrameworkCore;
using Mostaza.Dominio.Core.ContratosRepositorios;
using Mostaza.Dominio.Core.UnidadDeTrabajo;
using Mostaza.Infraestructura.ContextoDeDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mostaza.Infraestructura.Implementaciones
{
    public class UnidadDeTrabajo: IUnidadDeTrabajo, IDisposable
    {
        private readonly MostazaDbContext dataContext;
        private readonly IPerfilRepositorio _perfilRepositorio;
        private readonly IUsuarioRepositorio _usuarioRepositorio;


        public UnidadDeTrabajo(MostazaDbContext dataContext, IPerfilRepositorio perfilRepositorio, IUsuarioRepositorio usuarioRepositorio)
        {
            this.dataContext = dataContext;
            _perfilRepositorio = perfilRepositorio;
            _usuarioRepositorio = usuarioRepositorio;
        }
        public void Commit()
        {

            dataContext.SaveChanges();
            Dispose();
        }

        public void RejectChanges()
        {
            Dispose();
        }
        public void Dispose()
        {

            dataContext.Dispose();
        }
    }
}
