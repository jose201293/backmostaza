﻿using Mostaza.Dominio.Core.ContratosRepositorios;
using Mostaza.Dominio.Core.Entidades;
using Mostaza.Infraestructura.ContextoDeDatos;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mostaza.Infraestructura.Implementaciones
{
    public class DireccionRepositorio : IDireccionRepositorio
    {
        private readonly MostazaDbContext _contexto;

        public DireccionRepositorio()
        {
            
        }
        public async Task<Direcciones> CrearDireccionAsync(Direcciones direccion)
        {
            var direccionContext = await _contexto.AddAsync(direccion);

            return direccionContext.Entity;
        }

        public IEnumerable<Direcciones> ListarDirecciones()
        {
            throw new NotImplementedException();
        }
    }
}
