﻿using Mostaza.Dominio.Core.ContratosRepositorios;
using Mostaza.Dominio.Core.Entidades;
using Mostaza.Dominio.Core.UnidadDeTrabajo;
using Mostaza.Infraestructura.ContextoDeDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;

namespace Mostaza.Infraestructura.ImplementacionRepositorios
{
    public class UsuarioRepositorio : IUsuarioRepositorio
    {
        private readonly MostazaDbContext _contexto;
        public UsuarioRepositorio(MostazaDbContext contexto)
        {
            _contexto = contexto;
        }
        public async Task<Usuarios> CrearUsuario(Usuarios usuario)
        {
            try
            {
                var usuarioContext = await _contexto.AddAsync(usuario);
                return usuarioContext.Entity;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public void EliminarUsuario(int id)
        {
            throw new NotImplementedException();
        }
        public async Task<Usuarios> GetUsuarioAsync(Expression<Func<Usuarios, bool>> predicado)
        {
            var usuario = await _contexto.Usuarios.FirstOrDefaultAsync(predicado);
            return usuario;
        }
        public List<Usuarios> GetUsuarios()
        {
            var usuarios = _contexto.Usuarios.ToList();
            return usuarios;
        }
    }
}
