﻿using Microsoft.Extensions.DependencyInjection;
using Mostaza.Aplicacion.ContratosServicioAplicacion;
using Mostaza.Aplicacion.ServiciosAplicacion;
using Mostaza.Dominio.Core.ContratosRepositorios;
using Mostaza.Dominio.Core.ContratosServiciosDominio;
using Mostaza.Dominio.Core.ServiciosDeDominio;
using Mostaza.Dominio.Core.UnidadDeTrabajo;
using Mostaza.Infraestructura.ContextoDeDatos;
using Mostaza.Infraestructura.Implementaciones;
using Mostaza.Infraestructura.ImplementacionRepositorios;
using ServiciosTransversales.Mapper.Contratos;
using ServiciosTransversales.Mapper.Implementaciones;
using ServiciosTransversales.Seguridad.Contratos;
using ServiciosTransversales.Seguridad.Implementaciones;
using System;
using System.Collections.Generic;
using System.Text;

namespace IoC
{
    public static class Container
    {
        public static IServiceCollection CreacionDependencias(this IServiceCollection services)
        {
            services.AddTransient<IUsuarioRepositorio,UsuarioRepositorio>();
            services.AddTransient<IPerfilRepositorio, PerfilRepositorio>();
            services.AddTransient<IUnidadDeTrabajo, UnidadDeTrabajo>();
            services.AddTransient<IServicioPerfil, ServicioPerfil>();
            services.AddScoped<MostazaDbContext>();
            //capa de aplicacion
            services.AddTransient<IServicioAplicacionPerfil, ServicioAplicacionPerfil>();

            //transversales
            services.AddTransient<IJwtManager, JwtManager>();
            services.AddTransient<IMapperObject, Mapper>();

            
            return services;
        }
    }
}
