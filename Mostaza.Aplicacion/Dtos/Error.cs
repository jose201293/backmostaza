﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mostaza.Aplicacion.Dtos
{
    public class Error
    {
        public string Mensaje { get; set; }
        public string CodigoError { get; set; }
        public string EstadoError { get; set; }
    }
}
