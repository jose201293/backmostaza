﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mostaza.Aplicacion.Dtos
{
    public class PerfilDto
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public string Correo { get; set; }
        public string Contrasenia { get; set; }
    }
}
