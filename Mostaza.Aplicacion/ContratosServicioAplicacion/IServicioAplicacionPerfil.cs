﻿using Mostaza.Aplicacion.Dtos;
using System.Threading.Tasks;

namespace Mostaza.Aplicacion.ContratosServicioAplicacion
{
    public interface IServicioAplicacionPerfil
    {
        Task <object> CrearPerfilAsync(PerfilDto perfilDto);
    }
}