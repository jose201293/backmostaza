﻿using Mostaza.Aplicacion.ContratosServicioAplicacion;
using Mostaza.Aplicacion.Dtos;
using Mostaza.Dominio.Core.ContratosServiciosDominio;
using Mostaza.Dominio.Core.Entidades;
using ServiciosTransversales.Mapper.Contratos;
using ServiciosTransversales.Seguridad.Contratos;
using ServiciosTransversales.Utilidades.Constantes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mostaza.Aplicacion.ServiciosAplicacion
{
    public class ServicioAplicacionPerfil : IServicioAplicacionPerfil
    {
        private readonly IMapperObject _mapperObject;
        private readonly IServicioPerfil _servicioPerfil;
        private readonly IJwtManager _jwtManager;

        public ServicioAplicacionPerfil(IMapperObject mapperObject, IServicioPerfil servicioPerfil, IJwtManager jwtManager)
        {
            _mapperObject = mapperObject;
            _servicioPerfil = servicioPerfil;
            _jwtManager = jwtManager;
        }
        public async Task<object> CrearPerfilAsync(PerfilDto perfilDto)
        {
            perfilDto.Contrasenia = _jwtManager.EncriptarContrasenia(perfilDto.Contrasenia);
            Usuarios usuario = _mapperObject.CreateMap<PerfilDto, Usuarios>(perfilDto);
            Perfiles perfil = _mapperObject.CreateMap<PerfilDto, Perfiles>(perfilDto);
            var respuestaUsuario = await _servicioPerfil.CrearPerfilAsync(perfil, usuario);
            if (respuestaUsuario.Item1 != null)
            {
                return _jwtManager.GenerarToken(respuestaUsuario.Item1);
            }
            else
            {
                return new Error
                {
                    Mensaje = respuestaUsuario.Item2,
                    CodigoError = Contante.CodigoInternoServicioAplicacion,
                    EstadoError = System.Net.HttpStatusCode.InternalServerError.ToString()
                };
            }

        }
    }
}
