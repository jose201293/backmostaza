﻿using Mostaza.Dominio.Core.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mostaza.Dominio.Core.ContratosRepositorios
{
    public interface IDireccionRepositorio
    {
        Task<Direcciones> CrearDireccionAsync(Direcciones direccion);
        IEnumerable<Direcciones> ListarDirecciones();

    }
}
