﻿using Mostaza.Dominio.Core.Entidades;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mostaza.Dominio.Core.ContratosRepositorios
{
    public interface IPerfilRepositorio
    {
        List<Perfiles> GetPerfil(Expression<Func<Usuarios, bool>> predicado);
        Task<Perfiles> CrearPerfilAsync(Perfiles usuario);
        void EliminarPerfil(int id);
    }
}
