﻿using Mostaza.Dominio.Core.Entidades;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Mostaza.Dominio.Core.ContratosRepositorios
{
    public interface IUsuarioRepositorio
    {
        List<Usuarios> GetUsuarios();
        Task<Usuarios> CrearUsuario(Usuarios usuario);
        Task<Usuarios> GetUsuarioAsync(Expression<Func<Usuarios, bool>> predicado);
        void EliminarUsuario(int id);
    }
}
