﻿using Mostaza.Dominio.Core.ContratosRepositorios;
using Mostaza.Dominio.Core.ContratosServiciosDominio;
using Mostaza.Dominio.Core.Entidades;
using Mostaza.Dominio.Core.UnidadDeTrabajo;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mostaza.Dominio.Core.ServiciosDeDominio
{
    public class ServicioPerfil : IServicioPerfil
    {
        private readonly IPerfilRepositorio _perfilRepositorio;
        private readonly IUsuarioRepositorio _usuarioRepositorio;
        private readonly IUnidadDeTrabajo _unidadTrabajo;


        public ServicioPerfil(IPerfilRepositorio perfilRepositorio, IUsuarioRepositorio usuarioRepositorio, IUnidadDeTrabajo unidadTrabajo)
        {
            _perfilRepositorio = perfilRepositorio;
            _usuarioRepositorio = usuarioRepositorio;
            _unidadTrabajo = unidadTrabajo;
        }
        public async Task<Tuple<Usuarios,string>> CrearPerfilAsync(Perfiles perfil, Usuarios usuario)
        {
            usuario.IdEstadoUsuario = 1;
            usuario.IdTipoUsuario = 1;
            try
            {
                var usuarioExiste = await _usuarioRepositorio.GetUsuarioAsync(x=> x.Correo == usuario.Correo);
                if (usuarioExiste == null)
                {
                    var usuarioSave = await _usuarioRepositorio.CrearUsuario(usuario);
                    perfil.Usuario = usuarioSave;
                    var idPerf = await _perfilRepositorio.CrearPerfilAsync(perfil);
                    _unidadTrabajo.Commit();
                    return new Tuple<Usuarios,string>(usuarioSave,"Usuario regstrado");
                }
                return new Tuple<Usuarios, string>(null, "usuario existente");
            }
            catch(Exception ex) 
            {

                _unidadTrabajo.RejectChanges();
                return new Tuple<Usuarios, string>(null, "Error interno");

            }
        }
    }
}
