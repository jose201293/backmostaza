﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Mostaza.Dominio.Core.Entidades
{
    public class Usuarios
    {
        [Key]
        public int IdUsuario { get; set; }
        public string Correo { get; set; }
        public string Contrasenia { get; set; }
        public string Token { get; set; }
        public int IdEstadoUsuario { get; set; }
        public int IdTipoUsuario { get; set; }
        public Perfiles Perfil { get; set; }

    }


}
