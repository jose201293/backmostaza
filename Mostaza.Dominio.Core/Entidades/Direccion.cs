﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mostaza.Dominio.Core.Entidades
{

    public class Direcciones
    {
        public int IdDireccion { get; set; }
        public string Direccion { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }
        public string Referencia { get; set; }
        public string Telefono { get; set; }
        public string Telefono2 { get; set; }
        public string Ciudad { get; set; }
        public int IdPerfil { get; set; }
        public string Tag { get; set; }
        public byte DireccionPrincipal { get; set; }
    }
}
