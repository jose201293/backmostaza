﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Mostaza.Dominio.Core.Entidades
{
    public class Perfiles
    {
        [Key]
        public int IdPerfil { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Telefono { get; set; }
        public byte[] Fotografia { get; set; }
        public int IdUsuario { get; set; }
        public Usuarios Usuario  { get; set; }

    }
}
