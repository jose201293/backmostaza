﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mostaza.Dominio.Core.UnidadDeTrabajo
{
    public interface IUnidadDeTrabajo:IDisposable
    {
        void Commit();
        void RejectChanges();
    }
}
