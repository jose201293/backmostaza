﻿using Mostaza.Dominio.Core.Entidades;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Mostaza.Dominio.Core.ContratosServiciosDominio
{
    public interface IServicioPerfil
    {
        Task<Tuple<Usuarios, string>> CrearPerfilAsync(Perfiles perfil, Usuarios usuario);
    }
}
