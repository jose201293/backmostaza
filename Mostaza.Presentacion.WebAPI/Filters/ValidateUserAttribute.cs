﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Mostaza.Presentacion.WebAPI.Models;
using ServiciosTransversales.Seguridad.Contratos;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Mostaza.Presentacion.WebAPI.Filters
{
    public class ValidateUserAttribute : IAsyncActionFilter
    {
        private readonly IJwtManager _jwtManager;
        public ValidateUserAttribute(IJwtManager jwtManager)
        {
            _jwtManager = jwtManager;
        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
           
            //var usuarioModel = (UsuarioLoginModel)context.ActionArguments.SingleOrDefault(p => p.Value is UsuarioLoginModel).Value;
            //var usuario = usuarioModel.Usuario;
            //var contrasenia = usuarioModel.Contrasenia;
            //var usuarioValido = await _jwtManager.ValidarUsuarioAsync(usuario, contrasenia);

            //if (!usuarioValido)
            //{
            //    context.Result = new UnauthorizedResult();

            //    return;

            //}
            var result = await next();

        }
       
    }
}
