﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mostaza.Presentacion.WebAPI.Filters;
using Mostaza.Presentacion.WebAPI.Models;
using ServiciosTransversales.Seguridad.Contratos;
using ServiciosTransversales.Seguridad.Modelos;

namespace Mostaza.Presentacion.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly IJwtManager _jwtManager;
        public TokenController(IJwtManager jwtManager)
        {
            _jwtManager = jwtManager;
        }
        [HttpPost]
        //[ServiceFilter(typeof(ValidateUserAttribute))]
        public async Task<ActionResult<Token>> PostAsync(UsuarioLoginModel usuarioLoginModel)
        {
            var token = await _jwtManager.GenerarToken(usuarioLoginModel.Usuario, usuarioLoginModel.Contrasenia);
            if (token != null)
            {
                return Ok(token);
            }
            return Unauthorized();
        }
    }
}