﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mostaza.Aplicacion.ContratosServicioAplicacion;
using Mostaza.Aplicacion.Dtos;
using Mostaza.Presentacion.WebAPI.Models;
using ServiciosTransversales.Mapper.Contratos;

namespace Mostaza.Presentacion.WebAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {
        private readonly IServicioAplicacionPerfil _servicioAplicacionPerfil;
        private readonly IMapperObject _mapperObject;


        public UsuarioController(IServicioAplicacionPerfil servicioAplicacionPerfil, IMapperObject mapperObject)
        {
            _servicioAplicacionPerfil = servicioAplicacionPerfil;
            _mapperObject = mapperObject;
        }
        [HttpPost]
        public async Task<ActionResult<IEnumerable<string>>> PostAsync([FromBody]PerfilModel perfilModel)
        {
            var perfilDto = _mapperObject.CreateMap<PerfilModel, PerfilDto>(perfilModel);
            var respuesta = await _servicioAplicacionPerfil.CrearPerfilAsync(perfilDto);
            return Ok(respuesta);
        }
    }
}