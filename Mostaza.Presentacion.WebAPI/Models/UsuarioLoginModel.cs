﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mostaza.Presentacion.WebAPI.Models
{
    public class UsuarioLoginModel
    {
        public string Usuario { get; set; }
        public string Contrasenia { get; set; }

    }
}
